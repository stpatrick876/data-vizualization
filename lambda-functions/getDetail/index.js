"use strict";

const cryptoBase = "https://chasing-coins.com/api/v1/";

const enpoints = {
  top: `${cryptoBase}top-coins/`,
  details: `${cryptoBase}icos/lookup/`
};
module.exports.getDetail = async (event, context) => {
  const symbol = event.pathParameters.symbol;
  return await new Promise((resolve, reject) => {
    fetch(`${enpoints.details}${symbol}`)
      .then(resp => resp.json()) // Transform the data into json
      .then(function(data) {
        resolve({
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          },
          body: JSON.stringify(data)
        });
        // Create and append the li's to the ul
      })
      .catch(err => {
        reject({
          statusCode: 400,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          },
          error: JSON.stringify(err)
        });
      });
  });
};
