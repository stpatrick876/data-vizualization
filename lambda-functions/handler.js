"use strict";

const cryptoBase = "https://chasing-coins.com/api/v1/";

const enpoints = {
  top: `${cryptoBase}top-coins/`,
  details: `${cryptoBase}icos/lookup/`
};
module.exports.getTopCryptoCoinsData = async (event, context) => {
  const count = event.pathParameters.count;
  return await new Promise((resolve, reject) => {
    fetch(`${enpoints.top}${count}`)
      .then(resp => resp.json()) // Transform the data into json
      .then(data => {
        const arr = [];
        for (let key in data) {
          data[key]["place"] = key;
          arr.push(data[key]);
        }
        return arr;
      })
      .then(function(data) {
        resolve({
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          },
          body: JSON.stringify(data)
        });
        // Create and append the li's to the ul
      })
      .catch(err => {
        reject({
          statusCode: 400,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          },
          error: JSON.stringify(err)
        });
      });
  });
};
