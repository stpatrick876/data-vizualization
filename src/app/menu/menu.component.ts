import { Component, OnInit, AfterViewInit } from "@angular/core";
import { SplitTextMenu } from "./splitTextMenu";

@Component({
  selector: "data-vizualization-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit, AfterViewInit {
  menuItems: any[] = [
    {
      type: "Bar chart",
      label: "US GDP",
      path: "usGdpBar"
    },
    {
      type: "Scatter Plot Chart",
      label: "Bicycle Doping",
      path: "pbrScatterplot"
    },
    {
      type: "Pie Chart",
      label: "Top Crypto Coins",
      path: "topCryptoCoins"
    },
    {
      type: "chotopleth map",
      label: "US Educational Attainment"
    },
    {
      type: "Heat Map",
      label: "Global"
    },
    {
      type: "tree map",
      label: "Video Game Sales"
    },
    {
      type: "Line chart",
      label: "Average students age"
    }
  ];
  constructor() {}

  ngOnInit() {
    const delta = this.menuItems.length * 5;
    this.menuItems.forEach((item, i) => {
      item.offset = delta - i * 4;
    });
  }
  ngAfterViewInit() {
    SplitTextMenu.init();
  }
}
