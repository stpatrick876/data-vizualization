import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UsGdpBarComponent } from "./Charts/us-gdp-bar/us-gdp-bar.component";
import { CyclistDataScatterplotComponent } from "./Charts/cyclist-data-scatterplot/cyclist-data-scatterplot.component";
import { TopCoinsComponent } from "./Charts/top-coins/top-coins.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "usGdpBar",
    pathMatch: "full"
  },
  {
    path: "usGdpBar",
    component: UsGdpBarComponent
  },

  {
    path: "pbrScatterplot",
    component: CyclistDataScatterplotComponent
  },
  {
    path: "topCryptoCoins",
    component: TopCoinsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
