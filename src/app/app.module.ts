import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MenuComponent } from "./menu/menu.component";
import { UsGdpBarComponent } from "./Charts/us-gdp-bar/us-gdp-bar.component";
import { MatCardModule } from "@angular/material/card";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { DataService } from "./data-services/data.service";
import { CyclistDataScatterplotComponent } from "./Charts/cyclist-data-scatterplot/cyclist-data-scatterplot.component";
import { ChartContainerComponent } from "./chart-container/chart-container.component";
import { TopCoinsComponent } from './Charts/top-coins/top-coins.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ChartContainerComponent,
    UsGdpBarComponent,
    CyclistDataScatterplotComponent,
    TopCoinsComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
