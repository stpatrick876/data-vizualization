import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "data-vizualization-chart-container",
  templateUrl: "./chart-container.component.html",
  styleUrls: ["./chart-container.component.scss"]
})
export class ChartContainerComponent implements OnInit {
  @Input() title: string;
  @Input() chartType: string;
  @Input() loading: boolean;
  showChart: boolean;
  constructor() {}

  ngOnInit() {}
}
