import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { enpoints } from "./endpoints";
import { Observable, of, forkJoin } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { UsGdpData } from "../Charts/us-gdp-bar/UsGdpData";
import { CyclistData } from "../Charts/cyclist-data-scatterplot/CyclistData";
import { CryptoData } from "../Charts/top-coins/CryptoData";
declare const _: any;
@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private _httpClient: HttpClient) {}

  getUsGdpData(): Observable<UsGdpData[]> {
    return this._httpClient.get(enpoints.usGdp).pipe(
      map((res: any) => {
        const data = res.data;
        const byYear = _.chunk(data, 4);

        const formated = byYear.map((year, i) => {
          return year.map((d, i) => {
            const date = new Date(d[0]);
            return {
              date,
              value: d[1],
              quarter: i + 1
            };
          });
        });

        return _.flattenDeep(formated);
      })
    );
  }

  getCyclistData(): Observable<CyclistData[]> {
    const timeToSeconds = t => {
      const a = t.split(":");
      return a[0] * 60 + +a[1];
    };
    return this._httpClient.get(enpoints.cyclistData).pipe(
      map((res: any[]) =>
        res.map(d => {
          (d as CyclistData).timaInSeconds = timeToSeconds(d.Time);
          return d;
        })
      )
    );
  }

  getTopCryptoCoinsData(count: number) {
    return this._httpClient.get(`${enpoints.cryto}/${count}`).pipe(
      switchMap((data: any) => {
        const datailObsArr = data.map(d =>
          this._httpClient.get(`${enpoints.cryptoDetail}/${d.symbol}`)
        );
        return forkJoin(...datailObsArr).pipe(map(a => _.merge(data, a)));
      })
    );
  }
}
