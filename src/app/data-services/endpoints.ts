export const enpoints = {
  usGdp:
    "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json",
  cyclistData:
    "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/cyclist-data.json",
  cryto: "http://localhost:3000/getTopCryptoCoinsData",
  cryptoDetail: "http://localhost:3000/getTopCryptoCoinsDetail"
};

// " https://0cjbr069s2.execute-api.us-east-1.amazonaws.com/dev/getTopCryptoCoinsData",
//" https://0cjbr069s2.execute-api.us-east-1.amazonaws.com/dev/getTopCryptoCoinsDetail"
