import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsGdpBarComponent } from './us-gdp-bar.component';

describe('UsGdpBarComponent', () => {
  let component: UsGdpBarComponent;
  let fixture: ComponentFixture<UsGdpBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsGdpBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsGdpBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
