import { Component, OnInit } from "@angular/core";
import * as d3 from "d3";
import { DataService } from "../../data-services/data.service";
import { UsGdpData } from "./UsGdpData";
declare const _: any;
@Component({
  selector: "data-vizualization-us-gdp-bar",
  templateUrl: "./us-gdp-bar.component.html",
  styleUrls: ["./us-gdp-bar.component.scss"]
})
export class UsGdpBarComponent implements OnInit {
  margin = 60;
  width = 1100 - 2 * this.margin;
  height = 600 - 2 * this.margin;
  loading = false;
  constructor(private _dataSrv: DataService) {}

  ngOnInit() {
    this._dataSrv.getUsGdpData().subscribe((data: UsGdpData[]) => {
      this.drawChart(data);
      this.loading = false;
    });
  }

  drawChart(data): any {
    // Define chart
    const chart = d3
      .select("svg")
      .append("g")
      .attr("transform", `translate(${this.margin}, ${this.margin})`);

    // Define y axis scale
    const yScale = d3
      .scaleLinear()
      .range([this.height, 0])
      .domain([0, _.maxBy(data, "value").value]);

    chart.append("g").call(d3.axisLeft(yScale));

    // Define x axis scale
    const xScale = d3
      .scaleTime()
      .range([0, this.width])
      .domain([
        _.minBy(data, (d: UsGdpData) => d.date).date,
        _.maxBy(data, (d: UsGdpData) => d.date).date
      ]);

    chart
      .append("g")
      .attr("transform", `translate(0, ${this.height})`)
      .call(d3.axisBottom(xScale).tickFormat(d3.timeFormat("%Y")));

    // Define  bars
    chart
      .selectAll()
      .data(data)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", (d: UsGdpData) => xScale(d.date))
      .attr("y", (d: UsGdpData) => yScale(d.value))
      .attr("height", (d: UsGdpData) => this.height - yScale(d.value))
      .attr("fill", "#A5A30D")
      .attr("width", this.width / data.length)
      .on("mouseover", this.handleBarMouseOver)
      .on("mouseout", this.handleBarMouseOut);

    // Define text label
    chart
      .append("text")
      .attr("x", -(this.height / 2) + this.margin * 2)
      .attr("y", this.margin / 2.4)
      .attr("class", "y-text")
      .attr("transform", "rotate(-90)")
      .attr("text-anchor", "middle")
      .text("Gross Domestic Product");
  }

  handleBarMouseOver(d) {
    // Define tooltip
    const tooltipDiv = d3.select(".tooltip").style("opacity", 0);

    // highlight bar
    d3.select(this as any).style("fill", "#fff");

    // show tooltip
    tooltipDiv
      .transition()
      .duration(200)
      .style("opacity", 0.9);
    tooltipDiv
      .html(
        `<span>${d3.timeFormat("%Y")(d.date)} Q${d.quarter}</span><span>$${
          d.value
        } Billions</span>`
      )
      .style("left", d3.event.pageX - 100 + "px")
      .style("top", d3.event.pageY - 150 + "px");
  }
  handleBarMouseOut() {
    d3.select(this as any).style("fill", "#A5A30D");
    d3.select(".tooltip")
      .transition()
      .duration(500)
      .style("opacity", 0);
  }
}
