export class UsGdpData {
  date: Date;
  value: number;
  quarter?: 1 | 2 | 3 | 4;
}
