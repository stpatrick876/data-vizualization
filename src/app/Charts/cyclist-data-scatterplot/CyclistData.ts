export class CyclistData {
  Time: string;
  timaInSeconds: number;
  Place: number;
  Seconds: number;
  Name: string;
  Year: number;
  Nationality: string;
  Doping: string;
}
