import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CyclistDataScatterplotComponent } from "./cyclist-data-scatterplot.component";

describe("PbrScatterplotComponent", () => {
  let component: CyclistDataScatterplotComponent;
  let fixture: ComponentFixture<CyclistDataScatterplotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CyclistDataScatterplotComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyclistDataScatterplotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
