import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/data-services/data.service";
import { CyclistData } from "./CyclistData";
import * as d3 from "d3";

@Component({
  selector: "data-vizualization-cyclist-data-scatterplot",
  templateUrl: "./cyclist-data-scatterplot.component.html",
  styleUrls: ["./cyclist-data-scatterplot.component.scss"]
})
export class CyclistDataScatterplotComponent implements OnInit {
  margin = 60;
  width = 1100 - 2 * this.margin;
  height = 600 - 2 * this.margin;
  loading = true;
  constructor(private _dataSrv: DataService) {}

  ngOnInit() {
    this._dataSrv.getCyclistData().subscribe((data: CyclistData[]) => {
      this.drawChart(data);
      this.loading = false;
    });
  }
  drawChart(data: CyclistData[]): any {
    //Define color for fill
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    // Define chart
    const chart = d3
      .select("svg")
      .append("g")
      .attr("transform", `translate(${this.margin}, ${this.margin})`);

    const formatYaxisTick = s =>
      (s - (s %= 60)) / 60 + (9 < s ? ":" : ":0") + s;

    // Define y axis scale
    const yScale = d3
      .scaleLinear()
      .range([this.height, 0])
      .domain([
        d3.max(data.map((d: CyclistData) => d.timaInSeconds)),

        d3.min(data.map((d: CyclistData) => d.timaInSeconds))
      ]);

    chart.append("g").call(d3.axisLeft(yScale).tickFormat(formatYaxisTick));
    // Define text label
    chart
      .append("text")
      .attr("x", -(this.height / 2) + this.margin * 2)
      .attr("y", this.margin / 2.4 - this.margin - 10)
      .attr("class", "y-text")
      .attr("transform", "rotate(-90)")
      .attr("text-anchor", "start")
      .text("Time in Minutes");

    // Define x axis scale
    const xScale = d3
      .scaleLinear()
      .range([0, this.width])
      .domain([
        d3.min(data.map((d: CyclistData) => d.Year)),
        d3.max(data.map((d: CyclistData) => d.Year))
      ]);

    chart
      .append("g")
      .attr("transform", `translate(0, ${this.height})`)
      .call(d3.axisBottom(xScale).tickFormat(d3.format("d")));

    // draw dots
    chart
      .selectAll(".dot")
      .data(data)
      .enter()
      .append("circle")
      .attr("class", "dot")
      .attr("r", 7)
      .attr("cx", d => xScale(d.Year))
      .attr("cy", d => yScale(d.timaInSeconds))
      .style("fill", d => (d.Doping === "" ? color("4") : color("5")))
      .on("mouseover", function(d) {
        const tooltip = d3.select(".tooltip");

        tooltip
          .transition()
          .duration(200)
          .style("opacity", 0.9);
        tooltip
          .html(() => {
            const content = `<span>${d.Name}:${d.Nationality} </span>
                    <span>Year:${d.Year}, Time:${d.Time}</span>`;
            return d.Doping ? content + `<p>${d.Doping}</p>` : content;
          })
          .style("left", d3.event.pageX - 100 + "px")
          .style("top", d3.event.pageY - 208 + "px");
      })
      .on("mouseout", function(d) {
        const tooltip = d3.select(".tooltip");

        tooltip
          .transition()
          .duration(500)
          .style("opacity", 0);
      });

    // Define legend
    var legend = chart
      .selectAll(".legend")
      .data(color.domain())
      .enter()
      .append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        return "translate(0," + i * 20 + ")";
      });

    // draw legend colored rectangles
    legend
      .append("rect")
      .attr("x", this.width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

    // draw legend text
    legend
      .append("text")
      .attr("x", this.width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .attr("class", "legend-text")
      .text(function(d) {
        return d === "5"
          ? "Riders with dopin allegations"
          : "No dopin allegations";
      });
  }
}
