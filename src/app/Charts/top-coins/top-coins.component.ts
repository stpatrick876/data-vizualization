import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/data-services/data.service";
import * as d3 from "d3";
import { CryptoData } from "./CryptoData";

@Component({
  selector: "data-vizualization-top-coins",
  templateUrl: "./top-coins.component.html",
  styleUrls: ["./top-coins.component.scss"]
})
export class TopCoinsComponent implements OnInit {
  width: number = 600;
  height: number = 600;
  //margin = { top: 10, right: 10, bottom: 10, left: 10 };
  variable = "cap"; // value in data that will dictate proportions on chart
  category = "cap"; // compare data by
  padAngle = 5; // effectively dictates the gap between slices
  transTime = 100; // transition time
  activeCoin: any;
  showDetails = false;
  loading = true;
  constructor(private _dataSrv: DataService) {}

  ngOnInit() {
    this._dataSrv.getTopCryptoCoinsData(12).subscribe((data: CryptoData[]) => {
      console.log(data);
      if (data) {
        setTimeout(() => {
          this.drawChart(data);
        }, 1000);
        this.loading = false;
      }
    });
  }
  drawChart(data: any): any {
    const radius = Math.min(this.width, this.height) / 2;
    const colour = d3.scaleOrdinal(d3.schemeCategory10); // colour scheme

    const pie = d3
      .pie()
      .value(d => d["cap"])
      .sort(null);

    // contructs and arc generator. This will be used for the donut. The difference between outer and inner
    // radius will dictate the thickness of the donut
    const arc = d3
      .arc()
      .outerRadius(radius * 0.8)
      .innerRadius(radius * 0.6)
      .cornerRadius(3)
      .padAngle(0.015);

    // this arc is used for aligning the text labels
    const outerArc = d3
      .arc()
      .outerRadius(radius * 0.9)
      .innerRadius(radius * 0.9);

    const chart = d3
      .select("#cryptoChart")
      .attr("width", this.width)
      .attr("height", this.height)
      .append("g")
      .attr(
        "transform",
        "translate(" + this.width / 2 + "," + this.height / 2 + ")"
      );

    // g elements to keep elements within svg modular
    chart.append("g").attr("class", "slices");
    chart.append("g").attr("class", "labelName");
    chart.append("g").attr("class", "lines");

    const path = chart
      .select(".slices")
      .selectAll("path")
      .data(pie(data))
      .enter()
      .append("path")
      .attr("fill", d => colour(d.data["price"]))
      .attr("d", arc as any)
      .style("cursor", "pointer") // colour based on category mouse is over

      .on("click", d => this.showCoinDetails(d, colour));

    path

      .on("mouseenter", function(data) {
        chart
          .append("text")
          .attr("class", "toolCircle")
          .style("fill", "#fff") // colour based on category mouse is over
          .style("stroke", "#fff") // colour based on category mouse is over
          .style("font-size", "30px") // colour based on category mouse is over
          .attr("dy", -15) // hard-coded. can adjust this to adjust text vertical alignment in tooltip
          .html("Click Segment for Details") // add text to the circle.
          .style("font-size", ".7em")
          .style("text-anchor", "middle"); // centres text in tooltip

        chart
          .append("circle")
          .attr("class", "toolCircle")
          .attr("r", radius * 0.55) // radius of tooltip circle
          .style("fill", "#000") // colour based on category mouse is over
          .style("fill-opacity", 0.35);
      })
      .on("mouseout", function() {
        d3.selectAll(".toolCircle").remove();
      });
  }

  showCoinDetails(d: any, colour): void {
    this.activeCoin = d.data;
    this.activeCoin.bgc = colour(d.data["price"]);
    this.showDetails = true;
  }
}
