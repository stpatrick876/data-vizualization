import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "data-vizualization-loader",
  templateUrl: "./loader.component.html",
  styleUrls: ["./loader.component.scss"]
})
export class LoaderComponent implements OnInit {
  @Input() show: boolean;
  constructor() {}

  ngOnInit() {}
}
